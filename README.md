# Usage

At run time, a root directory must be specified with the environment variable `DATA_ROOT`.

In this root directory, each subdirectory provides the data source for one of more Datasets, as listed in the table below.

| Subdirectory        | Datasets                                 |
|---------------------|------------------------------------------|
| `apvrp_tilk`        | `crate::dataset::apvrp::{TILK, TILK_AB}` |
| `apvrp_tilk_b_part` | `crate::dataset::apvrp::TILK_B_PART`     |
| `apvrp_meisel`      | `crate::dataset::apvrp::MEISEL`          |
| `darp_cordeau`      | `crate::dataset::darp::CORDEAU_ORIG`     |
| `sdarp_riedler`     | `crate::dataset::darp::RIEDLER`          |
| `sdarp_hard`        | `crate::dataset::darp::HARD`             |
