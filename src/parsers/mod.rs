mod cordeau;
pub use cordeau::CordeauFmt;

mod meisel;
pub use meisel::MeiselFmt;

mod riedler;
pub use riedler::RiedlerFmt;

mod nom_prelude {
    pub use nom::{
        bytes::complete::tag,
        character::complete::*,
        combinator::*,
        error::{self, context, FromExternalError, ParseError},
        multi::*,
        number::complete::double,
        sequence::*,
        Finish, IResult, Parser,
    };
    pub use std::num::{ParseFloatError, ParseIntError};
    pub use std::str::FromStr;
}

mod common;

pub trait ParseInstance<Fmt>: Sized {
    fn parse(inputs: Fmt) -> crate::Result<Self>;
}
